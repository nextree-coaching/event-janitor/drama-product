package io.sample.product.flow.product.logic;

import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.janitor.EventStream;
import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.domain.event.ProductEvent;
import io.sample.product.event.product.ProductDomainEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ProductHandlerLogic {
    //
    private final EventStream eventStreamService;


    public void onProduct(ProductEvent event) {
        //
        ProductDomainEvent domainEvent = null;
        switch (event.getCqrsDataEventType()) {
            case Registered:
                domainEvent = newProduct(event);
                break;
            case Modified:
                domainEvent = ProductDomainEvent.genModifiedEvent(event.getProduct().getId(), event.getNameValues());
                break;
            case Removed:
                domainEvent = ProductDomainEvent.genRemovedEvent(event.getProduct().getId());
                break;
        }

        if(domainEvent != null) {
            eventStreamService.publishEvent(domainEvent);
            log.debug("ProductDomainEvent published========\n{}", domainEvent);
        }
    }

    private ProductDomainEvent newProduct(ProductEvent event) {
        //
        Product product = event.getProduct();

        return ProductDomainEvent.genRegisteredEvent(product.getId(),
                product.getName(),
                product.getDescription(),
                product.getCategory().toString(),
                product.getSpec().getColor().toHex(),
                product.getSpec().getSize(),
                product.getSpec().getMaterial().toString(),
                ActorKey.fromId(product.getActorId()));
    }
}
