/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.command.command;

import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.message.CqrsUserType;
import io.naraway.accent.domain.message.trace.TraceHeader;
import io.sample.product.aggregate.product.domain.entity.sdo.ProductCdo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommand;

import java.util.List;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.api.command.CqrsBaseCommandType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductCommand extends CqrsBaseCommand {
    //
    private ProductCdo productCdo;
    private List<ProductCdo> productCdos;
    private boolean multiCdo;
    private String productId;
    private NameValueList nameValues;

    protected ProductCommand(CqrsBaseCommandType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductCommand newRegisterProductCommand(ProductCdo productCdo) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Register);
        command.setProductCdo(productCdo);
        return command;
    }

    public static ProductCommand newRegisterProductCommand(List<ProductCdo> productCdos) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Register);
        command.setProductCdos(productCdos);
        command.setMultiCdo(true);
        return command;
    }

    public static ProductCommand newModifyProductCommand(String productId, NameValueList nameValues) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Modify);
        command.setProductId(productId);
        command.setNameValues(nameValues);
        return command;
    }

    public static ProductCommand newRemoveProductCommand(String productId) {
        /* Autogen by nara studio */
        ProductCommand command = new ProductCommand(CqrsBaseCommandType.Remove);
        command.setProductId(productId);
        return command;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductCommand fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductCommand.class);
    }

    public static ProductCommand sampleForRegister() {
        /* Autogen by nara studio */
        ProductCommand sample =  newRegisterProductCommand(ProductCdo.sample());
        TraceHeader sampleTrace = new TraceHeader(ActorKey.sample().getId(), CqrsUserType.Actor);
        sampleTrace.setService(null);
        sampleTrace.setMessage(null);
        sample.setTraceHeader(sampleTrace);

        return sample;
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sampleForRegister().toPrettyJson());
    }
}
