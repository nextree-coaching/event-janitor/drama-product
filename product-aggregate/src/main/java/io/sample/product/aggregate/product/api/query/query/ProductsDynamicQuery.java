/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.query.query;

import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.store.maria.jpo.ProductJpo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsDynamicQuery;
import java.util.List;

import io.naraway.accent.util.query.RdbQueryRequest;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;
import java.util.ArrayList;
import io.naraway.accent.domain.type.Offset;
import static java.util.Objects.nonNull;

@Getter
@Setter
@NoArgsConstructor
public class ProductsDynamicQuery extends CqrsDynamicQuery<List<Product>> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductJpo.class);
        Offset offset = getOffset();
        TypedQuery<ProductJpo> query = RdbQueryBuilder.build(request, offset);
        query.setFirstResult(offset.getOffset());
        query.setMaxResults(offset.getLimit());
        List<ProductJpo> productJpos = query.getResultList();
        setQueryResult(Optional.ofNullable(productJpos).map(jpos -> ProductJpo.toDomains(jpos)).orElse(new ArrayList<>()));
        if (nonNull(getOffset()) && getOffset().isTotalCountRequested()) {
            TypedQuery<Long> countQuery = RdbQueryBuilder.buildForCount(request);
            long totalCount = countQuery.getSingleResult();
            Offset countableOffset = getOffset();
            countableOffset.setTotalCount((int) totalCount);
            setOffset(countableOffset);
        }
    }
}
