package io.sample.product.aggregate.product.domain.entity.vo;

import io.naraway.accent.domain.ddd.ValueObject;
import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Spec implements ValueObject {
    //
    Color color;
    int size;
    Material material;

    public String toString() {
        //
        return toJson();
    }

    public static Spec fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Spec.class);
    }

    public static Spec sample() {
        //
        return new Spec(new Color(255, 0, 0),
                240,
                Material.cowhide);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample());
    }
}
