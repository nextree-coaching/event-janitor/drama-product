package io.sample.product.aggregate.product.domain.entity;

import io.naraway.accent.domain.ddd.DomainAggregate;
import io.naraway.accent.domain.ddd.StageEntity;
import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.type.NameValue;
import io.naraway.accent.domain.type.NameValueList;
import io.sample.product.aggregate.product.domain.entity.sdo.ProductCdo;
import io.sample.product.aggregate.product.domain.entity.vo.Category;
import io.sample.product.aggregate.product.domain.entity.vo.Spec;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Product extends StageEntity implements DomainAggregate {
    //
    private String name;
    private Category category;
    private String description;
    private Spec spec;

    public Product(String id, ActorKey actorKey) {
        //
        super(id, actorKey);
    }

    public Product(ProductCdo cdo) {
        //
        super(cdo.getActorKey());
        this.name = cdo.getName();
        this.description = cdo.getDescription();
        this.category = cdo.getCategory();
        this.spec = cdo.getSpec();
    }

    public static Product newInstance(ProductCdo cdo, NameValueList nameValues) {
        //
        Product product = new Product(cdo);
        product.modifyAttributes(nameValues);

        return product;
    }

    @Override
    protected void modifyAttributes(NameValueList nameValueList) {
        //
        for(NameValue nameValue : nameValueList.list()) {
            String value = nameValue.getValue();
            switch (nameValue.getName()) {
                case "description" :
                    description = value;
                    break;
                default:
                    throw new IllegalArgumentException(String.format("Attribure %s is not updatable.", nameValue.getName()));
            }
        }
    }
    public static Product sample() {
        //
        return new Product(ProductCdo.sample());
    }
}
