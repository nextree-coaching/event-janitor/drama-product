/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.command.rest;

import io.sample.product.aggregate.product.api.command.command.ProductCommand;
import io.sample.product.aggregate.product.domain.logic.ProductLogic;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

@RestController
@RequestMapping("/aggregate/product")
public class ProductResource implements ProductFacade {
    /* Autogen by nara studio */
    private final ProductLogic productLogic;

    public ProductResource(ProductLogic productLogic) {
        /* Autogen by nara studio */
        this.productLogic = productLogic;
    }

    @Override
    @PostMapping("/product/command")
    public CommandResponse executeProduct(@RequestBody ProductCommand productCommand) {
        /* Autogen by nara studio */
        return productLogic.routeCommand(productCommand).getCommandResponse();
    }
}
