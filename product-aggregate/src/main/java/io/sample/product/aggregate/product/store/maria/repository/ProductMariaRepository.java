/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.store.maria.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import io.sample.product.aggregate.product.store.maria.jpo.ProductJpo;

public interface ProductMariaRepository extends PagingAndSortingRepository<ProductJpo, String> {
    /* Autogen by nara studio */
}
