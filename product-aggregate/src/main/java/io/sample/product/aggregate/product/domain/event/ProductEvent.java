/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.domain.event;

import io.sample.product.aggregate.product.domain.entity.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.event.CqrsDataEvent;
import io.naraway.accent.domain.type.NameValueList;
import io.naraway.accent.domain.message.event.CqrsDataEventType;
import io.naraway.accent.util.json.JsonUtil;

@Getter
@Setter
@NoArgsConstructor
public class ProductEvent extends CqrsDataEvent {
    /* Autogen by nara studio */
    private Product product;
    private String productId;
    private NameValueList nameValues;

    protected ProductEvent(CqrsDataEventType type) {
        /* Autogen by nara studio */
        super(type);
    }

    public static ProductEvent newProductRegisteredEvent(Product product, String productId) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Registered);
        event.setProduct(product);
        event.setProductId(productId);
        return event;
    }

    public static ProductEvent newProductModifiedEvent(String productId, NameValueList nameValues, Product product) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Modified);
        event.setProductId(productId);
        event.setNameValues(nameValues);
        event.setProduct(product);
        return event;
    }

    public static ProductEvent newProductRemovedEvent(Product product, String productId) {
        /* Autogen by nara studio */
        ProductEvent event = new ProductEvent(CqrsDataEventType.Removed);
        event.setProduct(product);
        event.setProductId(productId);
        return event;
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductEvent fromJson(String json) {
        /* Autogen by nara studio */
        return JsonUtil.fromJson(json, ProductEvent.class);
    }
}
