/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.query.query;

import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.store.maria.jpo.ProductJpo;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsDynamicQuery;
import io.naraway.accent.util.query.RdbQueryRequest;
import io.naraway.accent.util.query.RdbQueryBuilder;
import javax.persistence.TypedQuery;
import java.util.Optional;

@Getter
@Setter
@NoArgsConstructor
public class ProductDynamicQuery extends CqrsDynamicQuery<Product> {
    /* Autogen by nara studio */

    public void execute(RdbQueryRequest<ProductJpo> request) {
        /* Autogen by nara studio */
        request.addQueryStringAndClass(genSqlString(), ProductJpo.class);
        TypedQuery<ProductJpo> query = RdbQueryBuilder.build(request);
        ProductJpo productJpo = query.getSingleResult();
        setQueryResult(Optional.ofNullable(productJpo).map(jpo -> jpo.toDomain()).orElse(null));
    }
}
