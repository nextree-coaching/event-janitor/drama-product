/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.store.maria;

import io.sample.product.aggregate.product.domain.entity.Product;
import org.springframework.stereotype.Repository;
import io.sample.product.aggregate.product.store.ProductStore;
import io.sample.product.aggregate.product.store.maria.repository.ProductMariaRepository;
import io.sample.product.aggregate.product.store.maria.jpo.ProductJpo;
import java.util.Optional;
import org.springframework.data.domain.Pageable;
import io.naraway.accent.domain.type.Offset;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Repository
public class ProductMariaStore implements ProductStore {
    /* Autogen by nara studio */
    private final ProductMariaRepository productMariaRepository;

    public ProductMariaStore(ProductMariaRepository productMariaRepository) {
        /* Autogen by nara studio */
        this.productMariaRepository = productMariaRepository;
    }

    @Override
    public void create(Product product) {
        /* Autogen by nara studio */
        ProductJpo productJpo = new ProductJpo(product);
        productMariaRepository.save(productJpo);
    }

    @Override
    public Product retrieve(String id) {
        /* Autogen by nara studio */
        Optional<ProductJpo> productJpo = productMariaRepository.findById(id);
        return productJpo.map(ProductJpo::toDomain).orElse(null);
    }

    @Override
    public void update(Product product) {
        /* Autogen by nara studio */
        ProductJpo productJpo = new ProductJpo(product);
        productMariaRepository.save(productJpo);
    }

    @Override
    public void delete(Product product) {
        /* Autogen by nara studio */
        productMariaRepository.deleteById(product.getId());
    }

    @Override
    public void delete(String id) {
        /* Autogen by nara studio */
        productMariaRepository.deleteById(id);
    }

    @Override
    public boolean exists(String id) {
        /* Autogen by nara studio */
        return productMariaRepository.existsById(id);
    }

    private Pageable createPageable(Offset offset) {
        /* Autogen by nara studio */
        if (offset.getSortDirection() != null && offset.getSortingField() != null) {
            return PageRequest.of(offset.page(), offset.limit(), (offset.ascendingSort() ? Sort.Direction.ASC : Sort.Direction.DESC), offset.getSortingField());
        } else {
            return PageRequest.of(offset.page(), offset.limit());
        }
    }
}
