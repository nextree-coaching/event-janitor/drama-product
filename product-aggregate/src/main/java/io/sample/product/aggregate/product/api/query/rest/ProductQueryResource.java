/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.query.rest;

import io.sample.product.aggregate.product.api.query.query.ProductDynamicQuery;
import io.sample.product.aggregate.product.api.query.query.ProductQuery;
import io.sample.product.aggregate.product.api.query.query.ProductsDynamicQuery;
import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.store.ProductStore;
import io.sample.product.aggregate.product.store.maria.jpo.ProductJpo;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naraway.accent.util.query.RdbQueryRequest;

import javax.persistence.EntityManager;
import io.naraway.accent.domain.message.api.query.QueryResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@RestController
@RequestMapping("/aggregate/product/product/query")
public class ProductQueryResource implements ProductQueryFacade {
    //
    private final ProductStore productStore;
    private final RdbQueryRequest<ProductJpo> request;

    public ProductQueryResource(ProductStore productStore, EntityManager entityManager) {
        /* Autogen by nara studio */
        this.productStore = productStore;
        this.request = new RdbQueryRequest<>(entityManager);
    }

    @Override
    @PostMapping("/")
    public QueryResponse<Product> execute(@RequestBody ProductQuery productQuery) {
        /* Autogen by nara studio */
        productQuery.execute(productStore);
        return productQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-single")
    public QueryResponse<Product> execute(@RequestBody ProductDynamicQuery productDynamicQuery) {
        /* Autogen by nara studio */
        productDynamicQuery.execute(request);
        return productDynamicQuery.getQueryResponse();
    }

    @Override
    @PostMapping("/dynamic-multi")
    public QueryResponse<List<Product>> execute(@RequestBody ProductsDynamicQuery productsDynamicQuery) {
        /* Autogen by nara studio */
        productsDynamicQuery.execute(request);
        return productsDynamicQuery.getQueryResponse();
    }
}
