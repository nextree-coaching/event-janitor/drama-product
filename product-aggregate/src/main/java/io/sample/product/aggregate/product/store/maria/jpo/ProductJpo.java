/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.store.maria.jpo;

import io.sample.product.aggregate.product.domain.entity.vo.Spec;
import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.domain.entity.vo.Category;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import io.naraway.accent.store.jpa.StageEntityJpo;

import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.Column;

import org.springframework.beans.BeanUtils;
import io.naraway.accent.util.json.JsonUtil;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "PRODUCT")
public class ProductJpo extends StageEntityJpo {
    //
    private String name; //
    @Enumerated(EnumType.STRING)
    private Category category;
    private String description;
    @Column(columnDefinition = "TEXT")
    private String specJson; // Spec

    public ProductJpo(Product product) {
        /* Autogen by nara studio */
        super(product);
        BeanUtils.copyProperties(product, this);
        this.specJson = JsonUtil.toJson(product.getSpec());
    }

    public Product toDomain() {
        /* Autogen by nara studio */
        Product product = new Product(getId(), genActorKey());
        BeanUtils.copyProperties(this, product);
        product.setSpec(Spec.fromJson(specJson));
        return product;
    }

    public static List<Product> toDomains(List<ProductJpo> productJpos) {
        /* Autogen by nara studio */
        return productJpos.stream().map(ProductJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        /* Autogen by nara studio */
        return toJson();
    }

    public static ProductJpo sample() {
        /* Autogen by nara studio */
        return new ProductJpo(Product.sample());
    }

    public static void main(String[] args) {
        /* Autogen by nara studio */
        System.out.println(sample());
    }
}
