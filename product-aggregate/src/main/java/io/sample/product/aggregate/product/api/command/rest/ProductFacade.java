/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.command.rest;

import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.sample.product.aggregate.product.api.command.command.ProductCommand;

public interface ProductFacade {
    /* Autogen by nara studio */
    CommandResponse executeProduct(ProductCommand productCommand);
}
