/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.domain.logic;

import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.domain.entity.sdo.ProductCdo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import io.sample.product.aggregate.product.store.ProductStore;
import io.naraway.janitor.EventStream;
import io.sample.product.aggregate.product.api.command.command.ProductCommand;
import io.naraway.accent.domain.message.api.command.CommandResponse;
import io.naraway.accent.domain.message.api.FailureMessage;
import java.util.Optional;

import io.sample.product.aggregate.product.domain.event.ProductEvent;
import io.naraway.accent.domain.type.NameValueList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.NoSuchElementException;

@Service
@Transactional
public class ProductLogic {
    //
    private final ProductStore productStore;
    private final EventStream eventStream;

    public ProductLogic(ProductStore productStore, EventStream eventStream) {
        /* Autogen by nara studio */
        this.productStore = productStore;
        this.eventStream = eventStream;
    }

    public ProductCommand routeCommand(ProductCommand command) {
        /* Autogen by nara studio */
        switch(command.getCqrsBaseCommandType()) {
            case Register:
                if (command.isMultiCdo()) {
                    List<String> entityIds = this.registerProducts(command.getProductCdos());
                    command.setCommandResponse(new CommandResponse(entityIds));
                } else {
                    String entityId = Optional.ofNullable(command.getNameValues()).filter(nameValueList -> command.getNameValues().size() != 0).map(nameValueList -> this.registerProduct(command.getProductCdo(), nameValueList)).orElse(this.registerProduct(command.getProductCdo()));
                    command.setCommandResponse(new CommandResponse(entityId));
                }
                break;
            case Modify:
                this.modifyProduct(command.getProductId(), command.getNameValues());
                command.setCommandResponse(new CommandResponse(command.getProductId()));
                break;
            case Remove:
                this.removeProduct(command.getProductId());
                command.setCommandResponse(new CommandResponse(command.getProductId()));
                break;
            default:
                command.setFailureMessage(new FailureMessage(new Throwable("CommandType must be Register, Modify or Remove")));
        }
        return command;
    }

    public String registerProduct(ProductCdo productCdo) {
        /* Autogen by nara studio */
        Product product = new Product(productCdo);
        if (productStore.exists(product.getId())) {
            throw new IllegalArgumentException("product already exists. " + product.getId());
        }
        productStore.create(product);
        ProductEvent productEvent = ProductEvent.newProductRegisteredEvent(product, product.getId());
        eventStream.publishEvent(productEvent);
        return product.getId();
    }

    public String registerProduct(ProductCdo productCdo, NameValueList nameValueList) {
        /* Autogen by nara studio */
        Product product = Product.newInstance(productCdo, nameValueList);
        if (productStore.exists(product.getId())) {
            throw new IllegalArgumentException("product already exists. " + product.getId());
        }
        productStore.create(product);
        ProductEvent productEvent = ProductEvent.newProductRegisteredEvent(product, product.getId());
        eventStream.publishEvent(productEvent);
        return product.getId();
    }

    public List<String> registerProducts(List<ProductCdo> productCdos) {
        /* Autogen by nara studio */
        return productCdos.stream().map(productCdo -> this.registerProduct(productCdo)).collect(Collectors.toList());
    }

    public Product findProduct(String productId) {
        /* Autogen by nara studio */
        Product product = productStore.retrieve(productId);
        if (product == null) {
            throw new NoSuchElementException("Product id: " + productId);
        }
        return product;
    }

    public void modifyProduct(String productId, NameValueList nameValues) {
        /* Autogen by nara studio */
        Product product = findProduct(productId);
        product.modify(nameValues);
        productStore.update(product);
        ProductEvent productEvent = ProductEvent.newProductModifiedEvent(productId, nameValues, product);
        eventStream.publishEvent(productEvent);
    }

    public void removeProduct(String productId) {
        /* Autogen by nara studio */
        Product product = findProduct(productId);
        productStore.delete(product);
        ProductEvent productEvent = ProductEvent.newProductRemovedEvent(product, product.getId());
        eventStream.publishEvent(productEvent);
    }

    public boolean existsProduct(String productId) {
        /* Autogen by nara studio */
        return productStore.exists(productId);
    }

    public void handleEventForProjection(ProductEvent productEvent) {
        /* Autogen by nara studio */
        switch(productEvent.getCqrsDataEventType()) {
            case Registered:
                productStore.create(productEvent.getProduct());
                break;
            case Modified:
                Product product = productStore.retrieve(productEvent.getProductId());
                product.modify(productEvent.getNameValues());
                productStore.update(product);
                break;
            case Removed:
                productStore.delete(productEvent.getProductId());
                break;
        }
    }
}
