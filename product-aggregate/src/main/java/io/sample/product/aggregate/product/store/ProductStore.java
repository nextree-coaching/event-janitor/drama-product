/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.store;

import io.sample.product.aggregate.product.domain.entity.Product;

public interface ProductStore {
    /* Autogen by nara studio */
    void create(Product product);
    Product retrieve(String id);
    void update(Product product);
    void delete(Product product);
    void delete(String id);
    boolean exists(String id);
}
