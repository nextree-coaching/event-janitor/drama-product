/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.query.rest;

import io.naraway.accent.domain.message.api.query.QueryResponse;
import io.sample.product.aggregate.product.domain.entity.Product;
import io.sample.product.aggregate.product.api.query.query.ProductQuery;
import io.sample.product.aggregate.product.api.query.query.ProductDynamicQuery;
import java.util.List;
import io.sample.product.aggregate.product.api.query.query.ProductsDynamicQuery;

public interface ProductQueryFacade {
    /* Autogen by nara studio */
    QueryResponse<Product> execute(ProductQuery productQuery);
    QueryResponse<Product> execute(ProductDynamicQuery productDynamicQuery);
    QueryResponse<List<Product>> execute(ProductsDynamicQuery productsDynamicQuery);
}
