package io.sample.product.aggregate.product.domain.entity.vo;

import io.naraway.accent.domain.ddd.ValueObject;
import io.naraway.accent.util.json.JsonUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Color implements ValueObject {
    //
    int red;
    int green;
    int blue;

    public String toString() {
        //
        return toJson();
    }

    public Color fromJson(String json) {
        //
        return JsonUtil.fromJson(json, Color.class);
    }

    public String toHex() {
        //
        return String.format("#%02x%02x%02x", red, green, blue);
    }
    public static Color sample() {
        //
        return new Color(255, 0, 0);
    }

    public static void main(String[] args) {
        //
        System.out.println(sample().toPrettyJson());
        System.out.println(sample().toHex());
    }
}
