/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.aggregate.product.api.query.query;

import io.sample.product.aggregate.product.domain.entity.Product;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naraway.accent.domain.message.api.query.CqrsBaseQuery;
import io.sample.product.aggregate.product.store.ProductStore;

@Getter
@Setter
@NoArgsConstructor
public class ProductQuery extends CqrsBaseQuery<Product> {
    /* Autogen by nara studio */
    private String productId;

    public void execute(ProductStore productStore) {
        /* Autogen by nara studio */
        setQueryResult(productStore.retrieve(productId));
    }
}
