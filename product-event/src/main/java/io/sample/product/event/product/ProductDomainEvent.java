package io.sample.product.event.product;

import io.naraway.accent.domain.key.stage.ActorKey;
import io.naraway.accent.domain.message.event.CqrsDataEventType;
import io.naraway.accent.domain.message.event.CqrsDomainEvent;
import io.naraway.accent.domain.type.NameValueList;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductDomainEvent extends CqrsDomainEvent {
    //
    private CqrsDataEventType eventType;
    private String id;
    private String name;
    private String description;
    private String category;
    private String color;
    private int size;
    private String material;
    private NameValueList nameValues;
    private ActorKey actorKey;

    public ProductDomainEvent(String id,
                              String name,
                              String description,
                              String category,
                              String color,
                              int size,
                              String material,
                              ActorKey actorKey) {
        //
        this.eventType = CqrsDataEventType.Registered;
        this.id = id;
        this.name = name;
        this.description = description;
        this.category = category;
        this.color = color;
        this.size = size;
        this.material = material;
        this.actorKey = actorKey;
    }

    public ProductDomainEvent(String id, NameValueList nameValues) {
        //
        this.eventType = CqrsDataEventType.Modified;
        this.id = id;
        this.nameValues = nameValues;
    }

    public ProductDomainEvent(String id) {
        //
        this.eventType = CqrsDataEventType.Removed;
        this.id = id;
    }

    public static ProductDomainEvent genRegisteredEvent(String id,
                                                        String name,
                                                        String description,
                                                        String category,
                                                        String color,
                                                        int size,
                                                        String material,
                                                        ActorKey actorKey) {
        //
        return new ProductDomainEvent(id, name, description, category, color, size, material, actorKey);
    }

    public static ProductDomainEvent genModifiedEvent(String id, NameValueList nameValues) {
        //
        return new ProductDomainEvent(id, nameValues);
    }

    public static ProductDomainEvent genRemovedEvent(String id) {
        //
        return new ProductDomainEvent(id);
    }
}
