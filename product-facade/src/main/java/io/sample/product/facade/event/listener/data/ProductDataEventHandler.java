package io.sample.product.facade.event.listener.data;

import io.naraway.janitor.event.EventHandler;
import io.sample.product.aggregate.product.domain.event.ProductEvent;
import io.sample.product.flow.product.logic.ProductHandlerLogic;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Component
@RequiredArgsConstructor
@Transactional
public class ProductDataEventHandler {
    //
    private final ProductHandlerLogic productHandlerLogic;

    @EventHandler
    public void handle(ProductEvent event) {
        //
        productHandlerLogic.onProduct(event);
    }
}
