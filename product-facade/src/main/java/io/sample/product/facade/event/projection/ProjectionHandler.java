/*
 COPYRIGHT (c) NEXTREE Inc. 2014
 This software is the proprietary of NEXTREE Inc.
 @since 2014. 6. 10.
*/
package io.sample.product.facade.event.projection;

import io.sample.product.aggregate.product.domain.logic.ProductLogic;
import io.naraway.accent.domain.message.event.broker.StreamEventMessage;
import io.sample.product.aggregate.product.domain.event.ProductEvent;

public class ProjectionHandler {
    /* Autogen by nara studio */
    private final ProductLogic productLogic;

    public ProjectionHandler(ProductLogic productLogic) {
        /* Autogen by nara studio */
        this.productLogic = productLogic;
    }

    public void handle(StreamEventMessage streamEventMessage) {
        /* Autogen by nara studio */
        String classFullName = streamEventMessage.getPayloadClass();
        String payload = streamEventMessage.getPayload();
        String eventName = classFullName.substring(classFullName.lastIndexOf(".") + 1);
        switch(eventName) {
            case "ProductEvent":
                ProductEvent productEvent = ProductEvent.fromJson(payload);
                productLogic.handleEventForProjection(productEvent);
                break;
        }
    }
}
